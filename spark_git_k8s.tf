# TODO: replace the values having <> with your own values
# It concerns the project_id, the region, the bucket for backend and other buckets

# GCP Variables
variable "project_id" {
  description = "Google Project ID."
  type        = string
  default     = "valiant-acumen-288222"
}
variable "region" {
  description = "Google Cloud region"
  type        = string
  default     = "europe-west3"
}
variable "credentials" {
  description = "Google Cloud credentials"
  type        = string
  default     = "token.json"
}
variable "input_bucket" {
  type        = string
  default     = "input-data-workflow-automation-project-cicd-k8s"
}
variable "output_bucket" {
  type        = string
  default     = "output-data-workflow-automation-project-cicd"
}
variable "archive_bucket" {
  type        = string
  default     = "input-data-workflow-automation-project-cicd"
}
variable "dataproc_bucket" {
  type        = string
  default     = "dataproc-bucket-k8s-metadata-cicd"
}
variable "backend_bucket" {
  type        = string
  default     = "gitlab-k8s-cicd-project-state-tf"
}

# Terraform backend
terraform {
  backend "gcs" {
    credentials = "token.json"
    bucket = "gitlab-k8s-cicd-project-state-tf"
  }
}

# Specify the GCP Providers
provider "google" {
  project     = var.project_id
  region      = var.region
  credentials = file("./${var.credentials}")
}
provider "google-beta" {
  project     = var.project_id
  region      = var.region
  credentials = file("./${var.credentials}")
}

# API Enable
resource "google_project_service" "composer_api" {
  project                    = var.project_id
  service                    = "composer.googleapis.com"
  disable_dependent_services = true
}
resource "google_project_service" "compute_api" {
  project                    = var.project_id
  service                    = "compute.googleapis.com"
  disable_dependent_services = true
}
resource "google_project_service" "dataproc_api" {
  project                    = var.project_id
  service                    = "dataproc.googleapis.com"
  disable_dependent_services = true
}
resource "google_project_service" "kubernetes_api" {
  project                    = var.project_id
  service                    = "container.googleapis.com"
  disable_dependent_services = true
}

# GCP Resources
resource "google_storage_bucket" "input_bucket" {
  name     = var.input_bucket
  location = var.region
  project  = var.project_id
}
resource "google_storage_bucket" "output_bucket" {
  name     = var.output_bucket
  location = var.region
  project  = var.project_id
}
resource "google_storage_bucket" "archive_bucket" {
  name     = var.archive_bucket
  location = var.region
  project  = var.project_id
}
resource "google_storage_bucket" "dataproc_bucket" {
  name     = var.dataproc_bucket
  location = var.region
  project  = var.project_id
}
resource "google_container_cluster" "spark_gitlab_cicd_gke_cluster" {
  provider           = google-beta
  depends_on         = [google_project_service.kubernetes_api]
  name               = "gke-spark-k8s-dataproc"
  location           = "europe-west3"
  node_locations     = ["europe-west3-b", "europe-west3-c"]
  initial_node_count = 2
  master_auth {
    username = ""
    password = ""
    client_certificate_config {
      issue_client_certificate = false
    }
  }
  node_config {
    machine_type = "e2-micro"
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform",
    ]
    workload_metadata_config {
      node_metadata = "EXPOSE"
    }
    preemptible = true
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
  timeouts {
    create = "30m"
    update = "40m"
  }
}