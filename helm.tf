provider "helm" {
  kubernetes {
    config_path = "./config_runner"
  }
}

resource "helm_release" "runner" {
  chart      = "gitlab-runner"
  name       = "gitlab-runner"
  values     = [file("values-runner.yaml")]
  repository = "https://charts.gitlab.io"
}

resource "helm_release" "runner_spark" {
  chart      = "gitlab-runner"
  name       = "spark-gitlab-runner"
  values     = [file("values-spark.yaml")]
  repository = "https://charts.gitlab.io"
}