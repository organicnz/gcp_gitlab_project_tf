variable "terraform_sa" {
  type = string
#   default     = "terraform-admin-sa@valiant-acumen-288222.iam.gserviceaccount.com"
}

resource "google_composer_environment" "composer_cluster_project" {
  name = "composer-cluster-spark-workflow-project-cicd"
  region = "europe-west3"
  project = var.project_id
  config {
    node_count = 4

    node_config {
      zone = "europe-west3-b"
      machine_type = "e2-micro"
      network    = google_compute_network.network.id
      subnetwork = google_compute_subnetwork.subnetwork.id
      service_account = var.terraform_sa
    }
  }
  depends_on = [google_compute_subnetwork.subnetwork]
}

resource "google_compute_network" "network" {
  name                    = "composer-network-project"
  auto_create_subnetworks = false
  project = var.project_id
}

resource "google_compute_subnetwork" "subnetwork" {
  project = var.project_id
  name          = "composer-subnetwork-project"
  ip_cidr_range = "10.2.0.0/16"
  region        = "europe-west3"
  network       = google_compute_network.network.id
}